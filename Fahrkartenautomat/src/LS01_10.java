import java.util.Scanner;

public class LS01_10 {
	
		public static Scanner tastatur = new Scanner(System.in);

		public static void main(String[] args) {
			while(true) {
		       double eingezahlterGesamtbetrag;
		       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		       
		       // Geldeinwurf
		       // -----------
		       eingezahlterGesamtbetrag = geldeinwurf(zuZahlenderBetrag);
		
		       // Fahrscheinausgabe
		       // -----------------
		       fahrscheinausgabe();
		       
		       // R�ckgeldberechnung und -Ausgabe
		       // -------------------------------
		       rueckgeldberechnung(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		       
			}
	    }

		public static double fahrkartenbestellungErfassen() {
			double gesamtBetrag = 0;
			int anzahlKarten = 1;

			
			int ticket = 0;
			do{
				System.out.println("\n\nW�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus: ");
				System.out.println(" Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
				System.out.println(" Tageskarte Regeltarif AB [8,60 EUR] (2)");
				System.out.println(" Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
				System.out.println(" Bezahlen (9)");
				double zuZahlenderBetrag = 0;
				System.out.print("\nIhre Wahl: ");
				ticket = tastatur.nextInt();
				switch(ticket) {
					case 1: {
						zuZahlenderBetrag = 2.90;
						break;
					}
					case 2: {
						zuZahlenderBetrag = 8.60;
						break;
					}
					case 3: {
						zuZahlenderBetrag = 23.50;
						break;
					}
					case 9: {
						return gesamtBetrag;
					}
					default: {
						System.out.println("\t>>>falsche Eingabe<<<");
						break;
					}
				}
				
				boolean tickenPruefung = true;
				while (tickenPruefung) {
					System.out.print("Anzahl der Tickets (1 - 10): ");
					anzahlKarten = tastatur.nextInt();
					if (anzahlKarten > 0 && anzahlKarten <= 10) {
						tickenPruefung = false;
					}
				}

				gesamtBetrag += zuZahlenderBetrag * anzahlKarten;

			}while(ticket != 9);
		
			return gesamtBetrag;
		}
		
		//FahrkarteBezahlen
		public static double geldeinwurf(double zuZahlenderBetrag) {
			double eingezahlterGesamtbetrag = 0.0;
			double eingeworfeneM�nze = 0.0;
			while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
				System.out.printf("%s%.2f%s%n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " �");
				System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
				eingeworfeneM�nze = tastatur.nextDouble();
				eingezahlterGesamtbetrag += eingeworfeneM�nze;
			}
			return eingezahlterGesamtbetrag;
		}

		public static void fahrscheinausgabe() {
			System.out.println("\nFahrschein wird ausgegeben");

			for (int i = 0; i < 8; i++) {
				System.out.print("=");
				warte(250);
			}
			System.out.println("\n\n");
		}

		public static void warte(int millisekunde) {
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public static void rueckgeldberechnung(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
			double r�ckgabebetrag = Math.round((eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100.0) / 100.0;
			if (r�ckgabebetrag > 0.0) {
				System.out.printf("%s%.2f%s%n", "Der R�ckgabebetrag in H�he von ", r�ckgabebetrag, " EURO");
				System.out.println("wird in folgenden M�nzen ausgezahlt:");

				while (r�ckgabebetrag >= 2.0)  // 2 EURO-M�nzen
				{
					muenzeAusgeben(2, "EURO");
					r�ckgabebetrag -= 2.0;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
				{
					muenzeAusgeben(1, "EURO");
					r�ckgabebetrag -= 1.0;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
				{
					muenzeAusgeben(50, "Cent");
					r�ckgabebetrag -= 0.5;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
				{
					System.out.println("20 CENT");
					muenzeAusgeben(20, "CENT");
					r�ckgabebetrag -= 0.2;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
				{
					muenzeAusgeben(10, "CENT");
					r�ckgabebetrag -= 0.1;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
				{
					muenzeAusgeben(5, "CENT");
					r�ckgabebetrag -= 0.05;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
			}

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}

		public static void muenzeAusgeben(int betrag, String einheit) {
			System.out.println(betrag + " " + einheit);
			
		tastatur.close();	
		}

}
