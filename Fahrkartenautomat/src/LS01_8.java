import java.util.Scanner;

public class LS01_8 {
	
	    public static void main(String[] args)
	    {
	       Scanner tastatur = new Scanner(System.in);
	      
	       double zuZahlenderBetrag; 
	       double eingezahlterGesamtbetrag;
	       double eingeworfeneM�nze;
	       double r�ckgabebetrag;
	       int anzahlKarten;
	       
	       System.out.print("Zu zahlender Betrag (EURO): ");
	       zuZahlenderBetrag = tastatur.nextDouble();
	       System.out.print("Anzahl der Tickets: ");
	       anzahlKarten = tastatur.nextInt();
	       zuZahlenderBetrag = zuZahlenderBetrag * anzahlKarten;
	       
	       
	       eingezahlterGesamtbetrag = 0.00;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	  double zuZahlen = zuZahlenderBetrag - eingezahlterGesamtbetrag;
	    	  System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlen);
	    	   
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }

	       
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	      
	       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.00)
	       {
	    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.00;
	           }
	           while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.00;
	           }
	           while(r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.50;
	           }
	           while(r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.20;
	           }
	           while(r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.10;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	       tastatur.close();
	    }
}



