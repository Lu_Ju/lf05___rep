import java.util.Scanner;

public class LS01_14 {

		public static Scanner tastatur = new Scanner(System.in);

		public static void main(String[] args) {
			while(true) {
		       double eingezahlterGesamtbetrag;
		       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		       
		       // Geldeinwurf
		       // -----------
		       eingezahlterGesamtbetrag = geldeinwurf(zuZahlenderBetrag);
		
		       // Fahrscheinausgabe
		       // -----------------
		       fahrscheinausgabe();
		       
		       // R�ckgeldberechnung und -Ausgabe
		       // -------------------------------
		       rueckgeldberechnung(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		       
			}
	    }

		public static double fahrkartenbestellungErfassen() {
			double gesamtBetrag = 0;
			int anzahlKarten = 1;
			int ticket = 0;

			String[] fahrkartenBezeichnung = {
					"Einzelfahrschein Berlin AB",
					"Einzelfahrschein Berlin BC",
					"Einzelfahrschein Berlin ABC",
					"Kurzstrecke",
					"Tageskarte Berlin AB",
					"Tageskarte Berlin BC",
					"Tageskarte Berlin ABC",
					"Kleingruppen-Tageskarte Berlin AB",
					"Kleingruppen-Tageskarte Berlin BC",
					"Kleingruppen-Tageskarte Berlin ABC",
			};
			
			double[] fahrkartenPreis = {
					2.90,
					3.30,
					3.60,
					1.90,
					8.60,
					9.00,
					9.60,
					23.50,
					24.30,
					24.90,
			};
			
			int arrayLength = fahrkartenBezeichnung.length;
			
			
			/* 1 + 2. Welche Vorteile hat man durch die Arrays ?
			 * Das Erweitern des Fahrkartensortiments l�sst sich leichter/schneller umsetzen. Zudem ist die max. L�nge der Schleife durch das Array begrenzt 
			 * Es ist egal welches Array f�r die x-Achse (l�nge) genommen wird da beide gleich lang sind.
			 */
			
			do{
				System.out.printf("%-16s%-40s%-16s%n","Auswahlnummer", "Bezeichnung", "Preis in Euro");
				for(int x = 0; x < arrayLength; x++) {
					System.out.printf("%-16d%-40s%-4.2f�%n", (x+1), fahrkartenBezeichnung[x], fahrkartenPreis[x]);
				}
				System.out.printf("%n%-16d%-40s%n", (arrayLength+1), "Zum Bezahlen");
				
				double zuZahlenderBetrag = 0;
				System.out.print("\nWelches Ticket?: ");
				ticket = tastatur.nextInt();
				
				if(ticket != (arrayLength+1)) {
					zuZahlenderBetrag = fahrkartenPreis[ticket-1];
				
					boolean ticketPruefung = true;
					
					while (ticketPruefung) {
						System.out.print("\nAnzahl der Tickets (1 - 10): ");
						anzahlKarten = tastatur.nextInt();
						if (anzahlKarten > 0 && anzahlKarten <= 10) {
							ticketPruefung = false;
						}
					}
					gesamtBetrag += zuZahlenderBetrag * anzahlKarten;
				}
			}while(ticket != (arrayLength+1));
		
			return gesamtBetrag;
			
			
			/* 3. Vergleichen Sie die neue Implementierung mit der alten und erl�utern Sie die Vor- und Nachteile der jeweiligen Implementierung.
			 * Vorteile (Neu): vereinfachtes hinzuf�gen von weiteren tickets.
			 * Nachteile (Neu):
			 * Vorteile (Alt): Arrays sind schwerer zu migrieren -> ArrayOutOfBoundsException
			 * Nachteile (Alt): Umst�ndliches Scripten neu hinzuf�gen von Tickets 
			 */
		}
		
		//FahrkarteBezahlen
		public static double geldeinwurf(double zuZahlenderBetrag) {
			double eingezahlterGesamtbetrag = 0.0;
			double eingeworfeneM�nze = 0.0;
			while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
				System.out.printf("%s%.2f%s%n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " �");
				System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
				eingeworfeneM�nze = tastatur.nextDouble();
				eingezahlterGesamtbetrag += eingeworfeneM�nze;
			}
			return eingezahlterGesamtbetrag;
		}

		public static void fahrscheinausgabe() {
			System.out.println("\nFahrschein wird ausgegeben");

			for (int i = 0; i < 8; i++) {
				System.out.print("=");
				warte(250);
			}
			System.out.println("\n\n");
		}

		public static void warte(int millisekunde) {
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public static void rueckgeldberechnung(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
			double r�ckgabebetrag = Math.round((eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100.0) / 100.0;
			if (r�ckgabebetrag > 0.0) {
				System.out.printf("%s%.2f%s%n", "Der R�ckgabebetrag in H�he von ", r�ckgabebetrag, " EURO");
				System.out.println("wird in folgenden M�nzen ausgezahlt:");

				while (r�ckgabebetrag >= 2.0)  // 2 EURO-M�nzen
				{
					muenzeAusgeben(2, "EURO");
					r�ckgabebetrag -= 2.0;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
				{
					muenzeAusgeben(1, "EURO");
					r�ckgabebetrag -= 1.0;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
				{
					muenzeAusgeben(50, "Cent");
					r�ckgabebetrag -= 0.5;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
				{
					System.out.println("20 CENT");
					muenzeAusgeben(20, "CENT");
					r�ckgabebetrag -= 0.2;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
				{
					muenzeAusgeben(10, "CENT");
					r�ckgabebetrag -= 0.1;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
				while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
				{
					muenzeAusgeben(5, "CENT");
					r�ckgabebetrag -= 0.05;
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100.0) / 100.0;
				}
			}

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}

		public static void muenzeAusgeben(int betrag, String einheit) {
			System.out.println(betrag + " " + einheit);
		}

	
}
